require 'json'

ActiveRecord::Base.establish_connection({
  adapter: 'postgresql',
  host: 'localhost',
  database: 'iotta',
  password: 'iotta'
})

Dir['./db/migrate/*.rb'].each { |f| require f }
ActiveRecord::Migration[5.0].subclasses.reverse.each do |klass|
  klass.migrate(:up)
end

require './message'
require './device_status'

# Bail if anything goes wrong.
Thread.abort_on_exception = true

Thread.new do
  loop do
    MQTT::Client.connect(ENV['MQTT_URI'], clean_session: false, client_id: 'iotta-rb') do |c|
      c.subscribe('/devices/#' => 1, 'devices/#' => 1, 'hello' => 1)
      c.get do |topic, message|
        # Omit empty messages
        begin
          json = JSON.parse(message)
        rescue JSON::ParserError => e
          $stderr.puts e
          next
        end
        
        if topic == 'hello'
          device_mac = json['mac']
          timestamp = Time.now
          
          DeviceStatus.where(device_mac: device_mac).destroy_all
          DeviceStatus.create!(device_mac: device_mac, timestamp: timestamp, message: json)
        else
          _, device_mac, subtopic = *Regexp.compile("/([0-9a-f]{12})(?:/(.+))?").match(topic)
          timestamp = json["timestamp"] ? Time.at(json["timestamp"]) : Time.now
          Message.create!(device_mac: device_mac,
                          subtopic: subtopic,
                          timestamp: timestamp,
                          payload: json)
        end
      end
    end
  end
end

class App < Sinatra::Base
  set :logging, true
  set :show_exceptions, false

  PATH_LATEST_RE = /\/latest\/([a-f0-9]{12})/
  PATH_RE = /\/devices\/([a-f0-9]{12})/

  before do
    ActiveRecord::Base.logger = logger
    ActiveRecord::Base.logger.level = Logger::DEBUG
  end

  get "/robots.txt" do
    halt 200, {'Content-Type' => 'text/plain'}, <<EOF
User-agent: *
Disallow: /
EOF
  end

  get '/statuses' do
    stream = Enumerator.new do |y|
      Yajl::Encoder.encode(DeviceStatus.all) do |chunk|
        y << chunk
      end
    end
  end

  get PATH_LATEST_RE do |device_mac|
    q = Message.where(device_mac: device_mac).order(timestamp: :desc).to_sql
    puts q
    if message = Message.where(device_mac: device_mac).order(timestamp: :desc).first
      payload = message.payload.merge(timestamp: message.timestamp)
      Yajl::Encoder.encode(payload)
    else
      ""
    end
  end
  
  get PATH_RE do |device_mac|
    from = params['from'] && Time.at(params['from'].to_i)
    to = params['to'] && Time.at(params['to'].to_i)
    subtopic = params['subtopic']

    messages = Message.where(device_mac: device_mac).order(timestamp: :asc)
    messages = messages.where('timestamp >= ?', from) if from
    messages = messages.where('timestamp < ?', to) if to
    messages = messages.where(subtopic: subtopic)

    output = messages.pluck(:timestamp, :payload).map do |timestamp, payload|
      payload.merge(timestamp: timestamp)
    end

    stream = Enumerator.new do |y|
      Yajl::Encoder.encode(output) do |chunk|
        y << chunk
      end
    end
  end

  not_found do
    status 404
    'Not found'
  end
end
