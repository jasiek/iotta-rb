require 'rubygems'
require 'bundler'
Bundler.require
require './app'

use ActiveRecord::Rack::ConnectionManagement
use Rack::Cors do
  allow do
    origins /\Ahttp:\/\/localhost/
    resource '*'
  end
end
run App.new

