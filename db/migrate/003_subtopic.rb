class Subtopic < ActiveRecord::Migration[4.2]
  def change
    change_table :messages do |t|
      t.column :subtopic, :string
    end
  end
end
