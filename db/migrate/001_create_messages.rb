class CreateMessages < ActiveRecord::Migration[4.2]
  def change
    create_table :messages do |t|
      t.timestamp :timestamp
      t.string :device_mac
      t.json :payload
    end
  end
end
