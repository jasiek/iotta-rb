class Jsonb < ActiveRecord::Migration[4.2]
  def change
    execute "ALTER TABLE messages ALTER COLUMN payload TYPE jsonb;"
  end
end
