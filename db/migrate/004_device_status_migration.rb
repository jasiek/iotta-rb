class DeviceStatusMigration < ActiveRecord::Migration[4.2]
  def change
    create_table :device_statuses do |t|
      t.timestamp :timestamp
      t.string :device_mac
      t.jsonb :message
    end

    add_index :device_statuses, :device_mac
  end
end
